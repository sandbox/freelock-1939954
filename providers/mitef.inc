<?php

/**
 * @file
 *   This include processes MITEF video for use by the emfield.module
 *
 */

define('EMVIDEO_MITEF_MAIN_URL', 'http://video.mitef.org/');
define('EMVIDEO_MITEF_PLAYER_URL', 'http://video.mitef.com/media/swf/video-js.swf');
define('EMVIDEO_MITEF_ID_EMBED_URL', 'http://video.mitef.org/player/');
define('EMVIDEO_MITEF_INFO_URL', 'http://video.mitef.org/feeds/json/?shortcode=');

define('EMVIDEO_MITEF_DATA_VERSION', 1);


/**
 * hook emvideo_PROVIDER_info
 * this returns information relevant to a specific 3rd party video provider
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function emvideo_mitef_info() {
  $features = array(
    array(t('Autoplay'), t('Yes'), ''),
    //array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
    array(
      t('Full screen mode'),
      t('Yes'),
      t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'mitef',
    'name' => t('MITEF'),
    'url' => EMVIDEO_MITEF_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !provider.', array('!provider' => l(t('MITEF.com'), EMVIDEO_MITEF_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 */
function emvideo_mitef_settings() {
  $form['mitef']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen.
  $form['mitef']['player_options']['emvideo_mitef_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_mitef_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 * hook emvideo_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 * @param $code
 *   an optional string with the pasted URL or embed code
 * @return
 *   either an array of regex expressions to be tested, or a string with the video code to be used
 *   if the hook tests the code itself, it should return either the string of the video code (if matched), or an empty array.
 *   otherwise, the calling function will handle testing the embed code against each regex string in the returned array.
 */
function emvideo_mitef_extract($embed = '') {
  // http://video.mitef.org/84egJ
  // <iframe title="MIT Enterprise Forum Video Portal Video Player" width="640" height="360" src="http://video.mitef.org/player/84egJ" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  return array(
    '@video\.mitef\.org/([0-9a-zA-Z]+)$@i',
    '@video\.mitef\.org/player/([^"\?/]+)"@i',
  );
}

/**
 * hook emfield_PROVIDER_data
 *
 * provides an array to be serialised and made available with $item elsewhere
 */
function emvideo_mitef_data($field, $item) {
  // Initialize the data array.
  $data = array();
  $data['emvideo_mitef_version'] = EMVIDEO_MITEF_DATA_VERSION;

  // Gather info about the item's raw flash video.
  $url = EMVIDEO_MITEF_INFO_URL . $item['value'];
  $xml = emfield_request_xml('mitef', $url, array(), TRUE, FALSE, FALSE, FALSE, TRUE);

// [{"video_url": "http://video.mitef.org/transcode/redirects/370151.mp4", "pubdate": "2012-11-13T19:38:36", "headline": "Obstacles and Opportunities for Entrepreneurs in Education", "absolute_url": "http://video.mitef.org/RBkR/obstacles-and-opportunities-for-entrepreneurs-in-education/", "summary": "Sponsored Bill & Melinda Gates Foundation. - \r\n Industry veteran Frank Catalano will moderate an event on Obstacles and Opportunities for Entrepreneurs in Education.\r\nFor the uninitiated, the backstory involves a host of converging trends in education. There is the promise of tablet devices and laptops to deliver personalized digital content. There are new Common Core learning standards that intend to make student learning consistent from state to state.", "length": "01:43:49", "thumbnail": "http://vidcaster-media.s3.amazonaws.com/sites/5092/videos/247900/freeze/thumbs/120x68HDVAN.jpg"}]
  if (count($xml)) {
    $data += (array)$xml[0];
  }
  return $data;
}

/**
 *  hook emvideo_PROVIDER_rss
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_mitef_rss($item, $teaser = NULL) {

  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];

    return $file;
  }
}
//

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video @ the original provider site.
 */
function emvideo_mitef_embedded_link($video_code, $data) {
  return $data['absolute_url'];
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_mitef_thumbnail($field, $item, $formatter, $node, $width, $height) {
  if (isset($item['data']['thumbnail']))  {
    return $item['data']['thumbnail'];
  }
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_mitef_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_mitef_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_mitef_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_mitef_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 * The embedded flash displaying the MITEF video.
 */
function theme_emvideo_mitef_flash($item, $width, $height, $autoplay) {
  $output = '';
  
  $output .= '<iframe title="MIT Enterprise Forum Video Portal Video Player" width="' . $width . '" height="' . $height . '" src="http://video.mitef.org/player/' . $item['value'] ;
  if ($autoplay) {
    $output .= '/autoplay';
  }
  $output .= '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

  return $output;
}

/**
 * Implementation of hook_emfield_subtheme().
 * This returns any theme functions defined by this provider.
 */
function emvideo_mitef_emfield_subtheme() {
  $themes = array(
    'emvideo_mitef_flash'  => array(
      'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL,
        'autoplay' => NULL),
        'file' => 'providers/mitef.inc',
        'path' => drupal_get_path('module', 'media_mitef'),
      )
  );
  return $themes;
}

/**
 * Implementation of hook_emvideo_PROVIDER_content_generate().
 */
function emvideo_mitef_content_generate() {
  return array(
  // flv
    '[mitef id=2]',
    '[mitef id=219]',
    '[mitef id=316]',
    '[mitef id=65]',
  // mp4
    '[mitef id=1110]',
    '[mitef id=1135]',
    '[mitef id=1142]'
  );
}
